import React, { useState, useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Grid from './components/Grid/Grid'
import Keyboard from './components/Keyboard/Keyboard'
import Modal from './components/Modal/Modal'
import './App.css';
import { getWordSolution, isWordExist } from './utils/words';

function App() {
  const [currentGuess, setCurrentGuess] = useState('');
  const [guesses, setGuesses] = useState<string[]>([]);
  const [isGameWin, setIsGameWin] = useState(false);
  const [isGameLose, setGameLose] = useState(false);
  const [openModal, setOpenModal] = useState(false);
  const [gameState, setGameState] = useState<'WIN' | 'LOSE' | 'PLAYING'>('PLAYING');
  const [wordSolution, setWordSolution] = useState(() => getWordSolution());

  const onChar = (value: string) => {
    if (currentGuess.length < 5 && guesses.length < 6) {
      setCurrentGuess(`${currentGuess}${value}`)
    }
  }

  const onDelete = () => {
    setCurrentGuess(currentGuess.slice(0, -1))
  }

  const onEnter = () => {

    if (!(currentGuess.length === 5)) {
      return toast("Huruf Kurang!", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }

    if (!isWordExist(currentGuess)) {
      return toast("Kata tidak ditemukan!", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }

    if (currentGuess.length === 5 && guesses.length < 6 && !isGameWin) {
      setGuesses([...guesses, currentGuess])
      setCurrentGuess('')

      if (currentGuess === wordSolution) {
        setGameState('WIN');
        return setIsGameWin(true)
      }
    }

    if (guesses.length === 5) {
      setGameLose(true);
      setGameState('LOSE');
    }
  }

  useEffect(() => {
    if (isGameWin) {
      setOpenModal(true);
    }
    if (isGameLose) {
      setOpenModal(true);
    }

  }, [isGameWin, isGameLose]);

  const handleCloseModal = () => {
    setOpenModal(false);
    setGuesses([]);
    setWordSolution(() => getWordSolution());
    setIsGameWin(false)
  }

  return (
    <div className="App">
      <div>
        Solution: {wordSolution}
      </div>
      <Grid guesses={guesses} currentGuess={currentGuess} solution={wordSolution} />
      <Keyboard
        onChar={onChar}
        onDelete={onDelete}
        onEnter={onEnter}
        guesses={guesses}
        solution={wordSolution}
      />
      <Modal
        gameState={gameState}
        isOpen={openModal}
        handleClose={() => handleCloseModal()}
        totalAttemp={guesses.length}
      />
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </div>
  );
}

export default App;

import styled from 'styled-components';

export const RowStyled = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 0.25rem;
  font-weight: 700;
  font-size: 1.125rem;
  line-height: 1.75rem;
`;

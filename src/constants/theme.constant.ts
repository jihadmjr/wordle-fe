export const FONT_SIZE = {
  SMALL: '.625em',
  SUB: '.65em',
  BODY: '.75em',
  MED: '.875em',
  H4: '1em',
  H3: '1.2em',
  H2: '2em',
  H1: '2.5em',
  LARGE: '3em'
};

export const COLOR_STATUS = {
  CORRECT: 'rgb(34, 197, 94)',
  WRONG: 'rgb(148, 163, 184)',
  MISS: 'rgb(234, 179, 8)'
};

export const COLOR = {
  WHITE: '#FFFFFF',
  BLACK: '#000000',
  GREY: '#E5E7EB'
};

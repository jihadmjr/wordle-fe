export enum CHAR_STATUS {
  CORRECT = 'CORRECT',
  WRONG = 'WRONG',
  MISS = 'MISS'
}

export const TOTAL_ROW = 6;

export type T_CHAR_STATUS = 'CORRECT' | 'WRONG' | 'MISS';

export type T_ELIGIBLE_CHAR =
  | 'Q'
  | 'W'
  | 'E'
  | 'R'
  | 'T'
  | 'Y'
  | 'U'
  | 'I'
  | 'O'
  | 'P'
  | 'A'
  | 'S'
  | 'D'
  | 'F'
  | 'G'
  | 'H'
  | 'J'
  | 'K'
  | 'L'
  | 'Z'
  | 'X'
  | 'C'
  | 'V'
  | 'B'
  | 'N'
  | 'M';

export type T_ELIGIBLE_KEY = T_ELIGIBLE_CHAR | 'ENTER' | 'DELETE';

import { WORDS } from '../constants/wordList';

export const getWordSolution = () => {
  const random = Math.floor(Math.random() * WORDS.length);
  return WORDS[random].toUpperCase();
};

export const isWordValidate = (word: string) => {
  return WORDS.includes(word.toLowerCase());
};

export const isWordExist = (word: string) => {
  return WORDS.includes(word.toLowerCase());
};

import { CHAR_STATUS, T_CHAR_STATUS } from '../constants';

export const getCharStatus = (
  guesses: string[],
  solution: string
): { [key: string]: T_CHAR_STATUS } => {
  const charObj: { [key: string]: T_CHAR_STATUS } = {};

  guesses.forEach(word => {
    word.split('').forEach((letter, i) => {
      if (!solution.includes(letter)) {
        return (charObj[letter] = CHAR_STATUS.WRONG);
      }

      if (letter === solution[i]) {
        return (charObj[letter] = CHAR_STATUS.CORRECT);
      }

      if (charObj[letter] !== CHAR_STATUS.CORRECT) {
        return (charObj[letter] = CHAR_STATUS.MISS);
      }
    });
  });

  return charObj;
};

export const getGuessedStatus = (
  guess: string,
  solution: string
): T_CHAR_STATUS[] => {
  const splitSolution = solution.split('');
  const splitGuess = guess.split('');

  const solutionCharsTaken = splitSolution.map(_ => false);

  const statuses: T_CHAR_STATUS[] = Array.from(Array(guess.length));

  // handle all correct cases first
  splitGuess.forEach((letter, i) => {
    if (letter === splitSolution[i]) {
      statuses[i] = CHAR_STATUS.CORRECT;
      solutionCharsTaken[i] = true;
      return;
    }
  });

  splitGuess.forEach((letter, i) => {
    if (statuses[i]) return;

    if (!splitSolution.includes(letter)) {
      // handles the absent case
      statuses[i] = CHAR_STATUS.WRONG;
      return;
    }

    // now we are left with "present"s
    const indexOfPresentChar = splitSolution.findIndex(
      (x, index) => x === letter && !solutionCharsTaken[index]
    );

    if (indexOfPresentChar > -1) {
      statuses[i] = CHAR_STATUS.MISS;
      solutionCharsTaken[indexOfPresentChar] = true;
      return;
    } else {
      statuses[i] = CHAR_STATUS.WRONG;
      return;
    }
  });

  return statuses;
};

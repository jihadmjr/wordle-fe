import React from 'react'
import styled from 'styled-components';
import { ReactNode } from 'react'
import { T_CHAR_STATUS, T_ELIGIBLE_KEY } from '../../constants'
import { COLOR, COLOR_STATUS } from '../../constants/theme.constant';

interface Props {
  children?: ReactNode;
  value: T_ELIGIBLE_KEY;
  width?: number;
  status?: T_CHAR_STATUS;
  onClick: (value: T_ELIGIBLE_KEY) => void;
}

export const Key = ({
  children,
  status,
  width = 40,
  value,
  onClick,
}: Props) => {

  return (
    <KeyStyled status={status}
      style={{ width: `${width}px`, height: '58px' }}
      onClick={() => onClick(value)}
    >
      {children || value}
    </KeyStyled>
  )
}

interface IKeyProps {
  status: T_CHAR_STATUS | undefined
}

const KeyStyled = styled.div<IKeyProps>`
  width: 40px;
  height: 58px;
  font-weight: 700;
  font-size: 0.75rem;
  background-color: ${(p) => p.status ? COLOR_STATUS[p.status] : ''};
  border: 2px solid ${(p) => p.status ? COLOR_STATUS[p.status] : COLOR.GREY};
  display: flex;
  justify-content: center;
  align-items: center;
  margin-left: 0.125rem;
  margin-right: 0.125rem;
  border-radius: 0.25rem;

`
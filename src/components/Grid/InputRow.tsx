import { Cell } from './Cell'
import { RowStyled } from '../../styles'

interface Props {
  guess: string;
}

const InputRow = ({ guess }: Props) => {
  const splitGuess = guess.split('')
  const emptyCells = Array.from(Array(5 - splitGuess.length))

  return (
    <RowStyled>
      {splitGuess.map((letter, i) => (
        <Cell key={i} value={letter} />
      ))}
      {emptyCells.map((_, i) => (
        <Cell key={i} />
      ))}
    </RowStyled>
  )
}

export default InputRow;
import React from 'react'
import styled from 'styled-components';
import { T_CHAR_STATUS } from '../../constants'
import { COLOR_STATUS, COLOR } from '../../constants/theme.constant'

interface Props {
  value?: string
  status?: T_CHAR_STATUS
}

export const Cell = ({ value, status }: Props) => {

  return (
    <CellStyled status={status} >{value}</CellStyled>
  )
}

interface IGridProps {
  status: T_CHAR_STATUS | undefined
}

const CellStyled = styled.div<IGridProps>`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 3.5rem;
  height: 3.5rem;
  margin: 0 0.125rem;
  color: ${(p) => p.status ? COLOR.WHITE : COLOR.BLACK};
  border: 2px solid ${(p) => p.status ? COLOR_STATUS[p.status] : COLOR.GREY};
  border-radius: 0.25rem;
  background-color: ${(p) => p.status ? COLOR_STATUS[p.status] : ''};
`
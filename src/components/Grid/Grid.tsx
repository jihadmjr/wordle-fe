import React from 'react'
import styled from 'styled-components';
import { TOTAL_ROW } from '../../constants';
import EmptyRow from './EmptyRow';
import InputRow from './InputRow';
import GuessedRow from './GuessedRow';

interface Props {
  guesses: string[];
  currentGuess: string;
  solution: string;
}

const Grid = ({ guesses, currentGuess, solution }: Props) => {
  const emptyRows = TOTAL_ROW - 1
  const empties =
    guesses.length < emptyRows ? Array.from(Array(emptyRows - guesses.length)) : []

  return (
    <GridContainer>
      {guesses.map((guess, idx) => (
        <GuessedRow guess={guess} key={idx} solution={solution} />
      ))}
      {guesses.length < 6 && <InputRow guess={currentGuess} />}
      {empties.map((_, i) => (
        <EmptyRow key={i} />
      ))}
    </GridContainer>
  )
}

const GridContainer = styled.div`
  margin-top: 1.5rem;
`;


export default Grid
import { Cell } from './Cell'
import { RowStyled } from '../../styles'
import { getGuessedStatus } from '../../utils'

interface Props {
  guess: string;
  solution: string;
}

const GuessedRow = ({ guess, solution }: Props) => {
  const status = getGuessedStatus(guess, solution);

  return (
    <RowStyled>
      {guess.split('').map((letter, idx) => (
        <Cell key={idx} value={letter} status={status[idx]} />
      ))}
    </RowStyled>
  )
}

export default GuessedRow
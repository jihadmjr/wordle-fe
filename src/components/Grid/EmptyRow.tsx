import { Cell } from './Cell'
import { RowStyled } from '../../styles'

const EmptyRow = () => {
  const emptyCells = Array.from(Array(5))

  return (
    <RowStyled>
      {emptyCells.map((_, i) => (
        <Cell key={i} />
      ))}
    </RowStyled>
  )
}


export default EmptyRow
import React from 'react'
import styled from 'styled-components';

interface Props {
  isOpen: boolean;
  handleClose: () => void;
  totalAttemp: number;
  gameState: 'WIN' | 'LOSE' | 'PLAYING';

}

const Modal = ({ isOpen, handleClose, totalAttemp, gameState }: Props) => {

  const renderContent = () => {
    if (gameState === 'WIN') {
      return (
        <ModalStyled>
          <div>
            YEY MENANG DALAM {totalAttemp}/6 PERCOBAAN 🎉
          </div>
          <CloseButtonStyled onClick={handleClose}>
            TUTUP & MAIN LAGI
          </CloseButtonStyled>
        </ModalStyled>
      )
    } else {
      return (
        <ModalStyled>
          <div>
            KAMU KALAH
          </div>
          <CloseButtonStyled onClick={handleClose}>
            TUTUP & MAIN LAGI
          </CloseButtonStyled>
        </ModalStyled>
      )
    }
  }

  if (isOpen) {
    return (
      <>
        <ModalBackDrop />
        {renderContent()}
      </>
    )
  }
  return null

}


const ModalStyled = styled.div`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  padding: 1.25rem;
  width: 75%;
  background-color: #FFFFFF;
`

const ModalBackDrop = styled.div`
  width: 100vw;
  height: 100vh;
  position: absolute;
  top: 0;
  left: 0;
  background: rgba(0, 0, 0, 0.4);
`

const CloseButtonStyled = styled.div`
  margin: 0.5rem auto;
  padding: 0.25rem;
  border-radius: 0.25rem;
  background-color: rgb(79 70 229);
  color: #FFFFFF;
  font-weight: 700;
`;

export default Modal;